package main

import "fmt"

func main(){

	a := 200
	b := &a 	//Andpersand es el simbolo que se utiliza para acceder a la direccion de memoria
	fmt.Println(b) //Vemos la direccion de memoria
	fmt.Println(*b) //Vemos el valor que tiene en la direccion de memoria


	*b = 300
	fmt.Println(*b)
}

//Un puntero es una direccion de memoria