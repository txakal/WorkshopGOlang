package main

import "fmt"

func main(){
	//Un array es como una variable pero que puede tener varios valores a lavez en una especie de fila numerada.
	var pueblos [4]string
	pueblos[0] = "Urrestilla"
	pueblos[1] = "Azpeitia"
	pueblos[2] = "Beasain"
	pueblos[3] = "Tolosa"

	fmt.Println(pueblos[0])
	fmt.Println(pueblos[1]) 
	fmt.Println(pueblos[2]) 
	fmt.Println(pueblos[3]) 
	fmt.Println(pueblos)


	ciudades := [3]string{"Donostia", "Iruña", "Baiona"}
	fmt.Println(ciudades)
	

	
	fmt.Println(len(pueblos)) //Longitud del array
	fmt.Println(cap(pueblos)) //Capacidad del array

	for i := 0; i < len(pueblos); i++{
		fmt.Println(pueblos[i])
	}

	for p, v := range pueblos{  //range es una funcion que regresa el valor de pueblos a v y la posicion a p
		fmt.Println(p, v)
	}

	//la _ es para que no pete 
	for _, v := range pueblos{  //para que solo devuelva el valor y no la posicion
		fmt.Println(v)
	}

	for p, _ := range pueblos{  //para que solo devuelva la posicion
		fmt.Println(p)
	}

	//Los arrays son poco flexibles porque solamente permiten almacenar la cantidad de datos que sea especificada al momento de declararlos
}	