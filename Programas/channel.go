package main

import "fmt"

func strlen(s string, ch chan int){ //Se crea una funcion que recive un string y un channel
	ch <- len(s) //el operador <- vale para mandar algo al channel en este caso la longitud del string
}

func main(){

	c := make(chan int) //Se utiliza make para crear un chanel, int es el tipo
	go strlen("Joxemari", c)// Se manda los dos parametros a la funcion y utilizando go para crear la gorutine
	go strlen("Pepe", c)
	x, y := <-c, <-c //Aqui creamos dos variables donce se guarda lo que nos ha mandado los channel
	fmt.Println(x, y, x+y)

}
//https://tour.golang.org/concurrency/2

//Los canales son un mecanismo  que atraves de la gorutines  pueden sincronizar y comuniarse unos con otros

//LO que ha pasado es que primero nos ha devuelto el chanel de pepe poruqe es mas corto y segundo el de joxemari haunque las allamos puesto en otro orden en el main, esto es porque cambia la concurrencia