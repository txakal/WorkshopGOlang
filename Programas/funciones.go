package main

import "fmt"

func main(){
	//1
	saludar()//llama a la funcion y se ejecutara lo que hay dentro

	//2
	despedida := despedir() //Secrea una variable donde se guardara lo que devuelve  la funcion
	fmt.Println(despedida)

	//3
	localidad := vivir("Urrestilla") //AL llamar a la funcion hay que pasarle un string que dentro de la funcion sera la variable nombre.
	//Tambien se puede poner el mismo nombre que la variable de la funcion
	fmt.Println(localidad)

	//4
	mes, dia := naciminento()
	fmt.Println("Naciste el",dia,"de",mes)
}

//1
func saludar(){ //Crea una funcion llamada saludar que dentro se ejecuta un Println
	fmt.Println("Aupa")
}

//2
func despedir() string{ //Se crea una funcion que recive un string, hay que poner siempre el tipo de dato que se va a retornar
	return "Agur" //Devuelve un texto al string
}

//3
func vivir(pueblo string) string{ //recive el parametro nombre y hay que poner el tipo, tambien devuelve
	return fmt.Sprintf("Vivo en %s",pueblo)//esta funcion retorna un string en vez de imprimirso
}

//4
func naciminento() (string, int){//Este no recive parametros sino que devuelve.
	var(
		mes string
		dia int
	)

	fmt.Println("En que mes naciste?")
	fmt.Scanf("%s", &mes)
	fmt.Println("Que dia?")
	fmt.Scanf("%d", &dia)

	return mes, dia

}