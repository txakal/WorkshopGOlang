package main

import "fmt"

func main(){
	nombre := "Joxemari"

	for p, v := range nombre{
		fmt.Println("Posicion:",p," Valor:",v)
	}


	apellidos := []string{"Agirre","Fernandez","Elosegi"}
	
	for p, v := range apellidos{
		fmt.Println("Posicion:",p," Valor:",v)
	}
	for _, _ = range apellidos{ //Aqui no devuelve nada porque no estan la variables v y p y se repite el for todo lo largo del array
		fmt.Println("Hola")
	}
}