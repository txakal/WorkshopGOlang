package main

import "fmt"

func main(){

	//TODAS LAS VARIABLES HAY QUE UTILIZARLA, SINO ERROR!!

	var nombre string //Declara una varaible string manualmente
	nombre = "Joxemari" //Da valor a esa variable string
	fmt.Println(nombre) 

	apellido := "Lopez" //Decalara una variable que atomaticamente se convierte en string sin tener que hacerlo manualmente
	fmt.Println(apellido)

	edad := 64  //Decalra variable int
	fmt.Println(edad)

	edad = 65 //Cambia la edad de valor, pero ya no se puede volver a declarar
	fmt.Println(edad)

	var pueblo = "Urrestilla" //Tambien se buede decalrar una variable con var y sin tener que poner el tipo
	fmt.Println(pueblo)

	var(
		dia = 23
		mes = "Enero"
		año = 1955
	)
	fmt.Println("Fecha de nacimiento:",dia,"de",mes,"de",año)

	provincia, cd := "Gipuzkoa", 20738 //Decalrar 2 variables a la vez
	fmt.Println(provincia, cd)

	var latitud float32
	latitud =  43.1582
	longitud := -2.2452

	fmt.Println(latitud, longitud)


	var ec string
	fmt.Println("Estado civil?")
	fmt.Scanf("%s", &ec)
	fmt.Println("El estado civil de ", nombre, "es", ec)
}