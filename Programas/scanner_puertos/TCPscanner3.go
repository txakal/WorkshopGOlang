package main

import(
	"fmt"
	"net"
)
//Por cada puerto se crea una go rutine pero va demasiado rapida y no lo hace
func main(){
	for i := 1; i < 1024; i++ {
		go func(j int){
			address := fmt.Sprintf("scanme.nmap.org:%d", j)
			conn, err := net.Dial("tcp", address)
			if err != nil{
				return
			}
			conn.Close()
			fmt.Printf("%d open\n", j)
		}(i)
	}
}