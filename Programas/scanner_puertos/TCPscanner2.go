package main
import(
	"fmt"
	"net"
)

func main(){
	for i := 1; i <= 1024; i++{ //for bat 1024 aldiz (portuak berez 65535 dira)
		address := fmt.Sprintf("scanme.nmap.org:%d", i)//Sprintf erabitlzen zenbakia stringera pasatzen da
		conn, err := net.Dial("tcp", address) //Konexiao eginten da
		if err != nil{ //continue es un instruccion que omite la itineracion actual del ciclo y salta al siguiente
			continue
		}
		conn.Close() //ondo atera bada konexio itxi egiten da
		fmt.Printf("%d open\n", i) //eta irekita dagoen portuaren zenbakia ikusarazi
	}
}
