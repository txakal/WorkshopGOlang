package main

import(
	"fmt"
	"net"
	"sync"
)

func main(){
	var wg sync.WaitGroup //Es un contador sincronizado
	for i := 1; i < 1024; i++ {
		wg.Add(1)//Se incrementa el contador
		go func(j int){
			//Una declaración defer pospone la ejecución de una función hasta que regresa la función circundante.
			defer wg.Done() //reduce el contador 
			address := fmt.Sprintf("scanme.nmap.org:%d", j)
			conn, err := net.Dial("tcp", address)
			if err != nil{
				return
			}
			conn.Close()
			fmt.Printf("%d open\n", j)
		}(i)
	}
	wg.Wait()
}