package main
import(
	"fmt"
	"net"
	"sort"
)

func worker(ports, resutls chan int){	//La funcion worker recive dos canales
	for p := range ports{
		address := fmt.Sprintf("scanme.nmap.org:%d", p)
		conn, err :=net.Dial("tcp", address)
		if err != nil{
			resutls <- 0 //Si el puerto esta cerrado devuelve 0 a results
			continue //y vuelve a empezar el bucle
		}
		conn.Close()
		resutls <- p //Si esta abierto manda el numero de puerto
	}
}

func main(){
	ports := make(chan int, 100)	//se crea un channel con la capacidad maxima de 100 para comunicar el numero del puerto
	resutls := make(chan int)	//se crea otro channel para comunicar el resultado
	var openports []int 	//se crea un slice para guardar el resultado de los puertos abiertos

	for i := 0; i < cap(ports); i++{ //
		go worker(ports, resutls)
	}

	go func(){
		for i := 1; i <= 1024; i++{
			ports <- i
		}
	}()

	for i := 0; i <1024; i++{
		port := <-resutls
		if port != 0{
			openports =append(openports, port)
		}
	}

	close(ports)//Con close se comunica que el chanell se ha cerrado
	close(resutls)
	sort.Ints(openports)//La funcion sort vale para ordenar el slice openports
	for _, port := range openports{
		fmt.Printf("%d open\n", port)
	}
}