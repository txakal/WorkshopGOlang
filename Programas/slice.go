package main

import "fmt"

func main(){
	//Con diferencia de array, el slice es mas flexible y mas eficiente.
	frutas := []string{"fresa","manzana","platano"}
	fmt.Println(frutas)
	fmt.Println(frutas[0])

	var verduras []string
	fmt.Println(len(verduras)) //La longitud es 0
	verduras = append(verduras, "cebolla", "puerro")//La funcion append recive 2 parametros. El slice de que se hace referencia  y los valores que se quieran introducir
	for _, v := range verduras{
		fmt.Println(v)
	}
	fmt.Println(len(verduras)) //La longitud ha crecido en 2

	//La funcion make creamos un slice, tiene 3 paramteros: el tipo, la longitud y la capacidad
	legumbres := make([]string, 0, 16)

	fmt.Println(len(legumbres), cap(legumbres))

	legumbres = append(legumbres, "Alubias", "Lentejas", "Garbanzos")//Bale para añadir elementos a un slice
	for _, v := range legumbres{
		fmt.Println(v)
	}
	fmt.Println(len(legumbres), cap(legumbres))

	//https://blog.golang.org/slices-intro
}