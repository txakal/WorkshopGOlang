package main

import "fmt"

func main(){
	var edad int
	fmt.Println("Cuantos años tienes?")
	fmt.Scanf("%d", &edad)

	//Lehendabizi
	//if edad <18{
	//	fmt.Println("Eres menor de edad.")
	//}else{
	//	fmt.Println("Eres mayor de edad.")
	//}


	//Bigarren
	if edad < 18 && edad > 0 || edad == 12{
		fmt.Println("Eres menor de edad.")
	}else if edad < 21{
		fmt.Println("Eres mayor de edad pero en Estados Unidos no puedes trincar.")
	}else{
		fmt.Println("eres mayor de edad")
	}


	fmt.Println("Fin del programa.")
}