package main

import (
	"fmt"
	"strings"
	"time"
)

func main(){
	//nombre_lento("Joxemari")
	//fmt.Println("VA LENTO LOCO") //El codigo es sincrono entonces para que esto aparezca tiene que acabar la funcion

	go nombre_lento("Joxemari") //COn esto la palabra go lo que hace es separar la ejecucion de la funcion entonces el main sigue corriendo y cuando llega  a la ultima tarea se acaba
	fmt.Println("Va lento loco")
	
	nombre_lento("Joxemari")
	
	/*var wait string
	fmt.Scanln(&wait)*/

}

func nombre_lento(name string){ //Recive un paramerto llamado string con nombre name
	
	letras := strings.Split(name, "")	//Creamos un slice utilizamos la funcion Split de la libteria strings que divide el string en letras, luego pongo "" para separarlo letra por letra.

	for _,letra := range(letras){ 
		time.Sleep(1000 * time.Millisecond) //Estamos indicando que espere un segundo cada vez que entre en el for
		fmt.Println(letra)
	}
}
